package com.example.midterm_app_1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.example.midterm_app_1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        findViewById<Button>(R.id.button).setOnClickListener {
            var intent = Intent(this,HelloActivity::class.java)
            var name = binding.textView1.text.toString()
            Log.d("ImportName",name)
            intent.putExtra("name",name)
            this.startActivity(intent)

        }
    }
}